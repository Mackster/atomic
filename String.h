#pragma once

#include <string>
#include <iostream>

class String
{
private:
    std::string value = "";

public:
    uint length = 0;

    String() {}
    ~String() {}

    String(const char* _value) : value(_value) { length = value.size(); }
    String(std::string& _value) : value(_value) { length = value.size(); }
    String(std::string _value) : value(_value) { length = value.size(); }

    bool Contains(const char& c);
    bool Contains(String& str);

    static bool Contains(String& str, const char& c);
    static bool Contains(String& str1, String& str2);

    String& Substring(const int& start);
    String& Substring(const int& start, const int& sublength);

    char operator [] (const int& idx)
    {
        if(idx < 0)
        {
            return value[length + idx];
        }
        else
            return value[idx];
    }

    String operator += (const char& c)
    {
        return String(value + c);
    }

    String operator += (const String& str)
    {
        return String(value + str.value);
    }

    String operator -= (const char& c)
    {
        std::string newValue;

        for (char _c : value)
        {
            if(_c != c)
            {
                newValue += _c;
            }
        }
    }

    bool operator == (const String& str)
    {
        return value == str.value;
    }

    friend std::ostream& operator << (std::ostream& os, const String& str)
    {
        os << str.value;
    }
};