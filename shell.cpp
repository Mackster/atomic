#include "lexer.h"
#include "Commands.h"

#include <iostream>
#include <map>

int main()
{
	bool running = true;
	std::string module = "*main*>";
	
	std::string line;

	typedef void (*CommandFunction)();

	std::map<std::string, CommandFunction> commands{ {"#e", &Exit}, {"#c", &Clear} };

	while(running)
	{
		line = std::string(); //Reset the line
	    std::cout << module;
		std::getline(std::cin, line);

		std::map<std::string, CommandFunction>::iterator it = commands.find(line);
		if (it != commands.end())
		{
			//element found;
			it->second();
		}
		else if(line != "")
		{
			Lexer lex(line);

			std::vector<Token> lexedTokens = lex.Tokenize(); //Tokenize the input
			lexedTokens = lex.SmartTokenize(lexedTokens); //Determine type of tokens based on surrounding tokens

			std::cout << lex.ToString(lexedTokens) << std::endl;
		}	
	}
}
