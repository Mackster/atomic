#pragma once

void Exit()
{
	exit(0);
}

void Clear()
{
#ifdef _WIN32
	std::system("cls");
#else
	std::system("clear");
#endif // _WIN32
}