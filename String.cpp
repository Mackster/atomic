#include "String.h"

String& String::Substring(const int& start)
{
    String sub = "";

    if(start >= length)
        return sub;

    for (size_t i = start; i < length; i++)
    {
        sub += value[i];
    }

    return sub;
}

String& String::Substring(const int& start, const int& sublength)
{
    String sub = "";

    if(start < 0 || start + sublength >= length)
        return sub;

    for (size_t i = 0; i < length; i++)
    {
        sub += value[i + start];
    }
    
    return sub;
}

bool String::Contains(const char& c)
{
    for (char _c : value)
    {
        if (_c == c)
            return true;
    }

    return false;
}

bool String::Contains(String& str)
{
    if(str.length > length)
        return false;

    return (Substring(0, str.length) == str) || Contains(Substring(1), str);
}

bool String::Contains(String& str, const char& c) { return str.Contains(c); }
bool String::Contains(String& str1, String& str2) { return str1.Contains(str2); }