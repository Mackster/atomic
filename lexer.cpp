#include "lexer.h"

void Lexer::Advance()
{
	idx++;

	if(idx >= text.size())
	{
		currentChar = 0;
	}
	else
	{
		currentChar = text[idx];
	}
	
}

bool Lexer::Contains(std::string& match, char& test)
{
	for(char c : match)
	{
		if (c == test)
		   return true;
	}

	return false;
}

bool Lexer::Contains(std::vector<std::string>& list, std::string& match)
{
	for (std::string l : list)
		if (l == match)
			return true;

	return false;
}

std::vector<Token> Lexer::Tokenize()
{	
	idx = -1;
	Advance();

	std::vector<Token> tokens;

	while(idx < text.size())
    {
		SkipBlankSpaces();

		if(currentChar == ';')
		{
			tokens.emplace_back(Token(Token::Terminator, ";"));
		}
		else if(Contains(digits, currentChar))
		{
			tokens.emplace_back(MakeNumber());
		}
		else if(Contains(operators, currentChar))
		{
			tokens.emplace_back(Token(Token::Operator, currentChar));
		}
		else if (Contains(braces, currentChar))
		{
			Token t;

			switch (currentChar)
			{
				case '(':
					t = Token(Token::LParen, "(");
					break;
				case ')':
					t = Token(Token::RParen, ")");
					break;
				case '{':
					t = Token(Token::LBrace, "{");
					break;
				case '}':
					t = Token(Token::RBrace, "}");
					break;
			}

			tokens.emplace_back(t);
		}
		else
		{
			tokens.emplace_back(MakeWord());
		}
		
		Advance();
	}

	return tokens;
}

std::vector<Token> Lexer::SmartTokenize(std::vector<Token>& tokens)
{
	if (tokens.size() == 0)
		return tokens;

	Token prev = tokens[0];
	Token next;

	for (size_t i = 1; i < tokens.size(); i++)
	{
		if (i < tokens.size() - 1)
			next = tokens[i + 1];

		tokens[i] = ApplyRules(tokens[i], prev, next);

		prev = tokens[i];
	}

	return tokens;
}

Token Lexer::ApplyRules(Token& t, Token& prev, Token& next)
{
	for (TokenizerRule r : tokenizerRules)
	{
		if (Contains(Types, t.value))
		{
			t.type = Token::Type;
			return t;
		}
		else if(r.Match(prev, next) && t.type == Token::Unknown)
		{
			t.type = r.result;

			if (r.result == Token::Type)
			{
				Types.emplace_back(t.value);
			}

			return t;
		}
	}
	
	return t;
}

Token Lexer::MakeNumber()
{	
   	std::string value;
	int numDots = 0;
	value += currentChar;

	Advance();
	std::string localDigits = digits + ".";

	while (Contains(localDigits, currentChar))
	{
		if(currentChar == '.')
		{
			if(numDots == 1)
				break;
			
			numDots++;
		}

		value += currentChar;

		Advance();
	}

	idx--;

	if(value[value.size() - 1] == '.')
	{
		idx--;
		value.erase(value.size() - 1);
	}

	if(numDots == 0)
		return Token(Token::ConstantInteger, value);
	else
		return Token(Token::ConstantFloat, value);	
}

void Lexer::SkipBlankSpaces()
{
	while (currentChar == ' ')
	{
		Advance();
	}
}

Token Lexer::MakeWord()
{	
	std::string word = "";

	while (currentChar != ' ' && currentChar != 0 && !Contains(operators, currentChar))
	{
		word += currentChar;
		Advance();
	}

	idx--;

	if(Contains(keywords, word))
		return Token(Token::Keyword, word);
	else
		return Token(Token::Unknown, word);
}

std::string Lexer::ToString(std::vector<Token>& tokens)
{
	if (tokens.size() == 0)
	{
		return "";
	}

	std::string ret = "(" + tokens[0].ToString();

	for (int i = 1; i < tokens.size(); i++)
	{
		ret += ", " + tokens[i].ToString();
	}

	ret += ")";

	return ret;
}

void Lexer::LoadSettings()
{
	std::fstream settingsfile(TokenizerSettings);
	std::string line;

	while (std::getline(settingsfile, line))
	{
		tokenizerRules.emplace_back(ParseSetting(line));
	}
}

std::vector<std::string> Lexer::Split(std::string& text, char splittOn)
{
	std::vector<std::string> split;

	std::string temp;

	for (char c : text)
	{
		if (c == splittOn && temp.size() > 0)
		{
			split.emplace_back(temp);
			temp = "";
		}
		else
		{
			temp += c;
		}
	}

	if (temp.size() != 0)
		split.emplace_back(temp);

	return split;
}

TokenizerRule Lexer::ParseSetting(std::string& line)
{
	bool useNext = true;

	std::vector<std::string> parts = Split(line, ' ');

	parts[2].erase(std::remove(parts[2].begin(), parts[2].end(), '\r'), parts[2].end());

	if(parts.size() != 3) { /*TODO: Implement the error system so that invalid rules are not parsed */ }

	std::vector<std::string> prevParts = Split(parts[0], ':');
	std::vector<std::string> nextParts = {"???", "???"};

	if(parts[1] == "*")
	{
		useNext = false;
	}
	else
	{
		nextParts = Split(parts[1], ':');
	}

	if (prevParts[0] == "Terminator")
		prevParts[0] = ";";

	if (nextParts[0] == "Terminator")
		nextParts[0] = ";";

	return TokenizerRule(Token(prevParts[0], prevParts[1]), Token(nextParts[0], nextParts[1]), useNext, Token(parts[2], std::string()).type);
}