#pragma once

#include "core.h"

#include <map>
#include <vector>
#include <fstream>
#include <algorithm>

#define STANDARD_TOKENTYPES type == LParen || type == RParen || type == LBrace || type == RBrace || type == Terminator

struct Token
{
	enum TokenType { Keyword, ConstantInteger, ConstantFloat, Operator, Type, DataType, Unknown, LParen, RParen, LBrace, RBrace, Terminator, Variable };

	std::map<TokenType, std::string> tokenstrings = 
	{ {Keyword, "Keyword"}, {ConstantInteger, "Int"}, {ConstantFloat, "Float"}, {Operator, "Operator"},
	{Type, "Type"}, {Unknown, "???"}, {LParen, "LParenthesis"}, {RParen, "RParenthesis"}, {LBrace, "LBrace"},
	{RBrace, "RBrace"}, {Terminator, ";"}, {DataType, "DataType"}, {Variable, "Var" } };

	TokenType type = Unknown;
	std::string value;

	Token() {}

	Token(std::string& str) : value(str) {}
	Token(char& c) : value(1, c) {}
	Token(const char* str) : value(str) {}

	Token(TokenType _type, std::string& str) : type(_type), value(str) {}
	Token(TokenType _type, char& c) : type(_type), value(1, c) {}
	Token(TokenType _type, const char* str) : type(_type), value(str) {}

	Token(std::string& _type, std::string _value) : value(_value)
	{
		for (std::map<TokenType, std::string>::iterator iter = tokenstrings.begin(); iter != tokenstrings.end(); ++iter)
		{
			if(iter->second == _type)
				type = iter->first;
		}
	}

	std::string ToString()
	{
		if (STANDARD_TOKENTYPES)
		{
			return tokenstrings[type];
		}

		return tokenstrings[type] + ": " + value;
	}

	bool operator == (Token& l)
	{
		if (l.type == type && l.value == "*" || l.type == type && value == "*")
			return true;

		return l.type == type && l.value == value;
	}
};

struct TokenizerRule
{
	Token prev;
	Token next;

	Token::TokenType result = Token::Unknown;

	bool useNext = false;

	TokenizerRule(Token _prev, Token _next, Token::TokenType _result)
		: prev(_prev), next(_next), result(_result) { };

	TokenizerRule(Token _prev, Token _next, bool _useNext, Token::TokenType _result)
		: prev(_prev), next(_next), result(_result) { useNext = _useNext; };

	TokenizerRule() {};

	bool Match(Token& _prev, Token& _next)
	{
		if(!useNext)
		{
			return prev == _prev;
		}
		else
		{
			return prev == _prev && next == _next;
		}
	}

	std::string ToString()
	{
		return "(" + prev.ToString() + ", " + next.ToString() + ") => " + prev.tokenstrings[result];
	}
};

class Lexer
{
private:
	int idx = 0;
	std::string digits = "0123456789";
	std::string operators = "+-*/=<>&|!.;";
	std::string braces = "(){}";
	std::vector<std::string> keywords { "Extern", "Class", "Data", "in", "for", "while", "if" };

	std::vector<std::string> Types;

	std::string TokenizerSettings = "IntelligentTokenizerSettings.txt";
	std::vector<TokenizerRule> tokenizerRules;

	char currentChar = 0;
	std::string& text;

	bool Contains(std::string& match, char& test);
	bool Contains(std::vector<std::string>& list, std::string& match);
	Token MakeNumber();
	Token MakeWord();
	void Advance();
	void SkipBlankSpaces();

public:
	Lexer(std::string& _text) : text(_text) { LoadSettings(); }
	~Lexer() {};

	std::vector<Token> Tokenize();
	std::vector<Token> SmartTokenize(std::vector<Token>& tokens);
	std::string ToString(std::vector<Token>& tokens);

	void LoadSettings();
	TokenizerRule ParseSetting(std::string& line);
	Token ApplyRules(Token& t, Token& prev, Token& next);

	std::vector<std::string> Split(std::string& text, char splittOn);
};